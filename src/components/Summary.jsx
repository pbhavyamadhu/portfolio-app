import React from "react";
import Organization from "./Organization";
import "../styles/organization.css";

function Summary() {
  return (
    <React.Fragment>
      <div className="imgStyle summaryApp"></div>
      <div>
        <Organization />
      </div>
    </React.Fragment>
  );
}

export default Summary;
