import React from "react";
import { Link } from "react-router-dom";

import myData from "../data/myData.json";
import "../styles/App.css";
import image from "../images/image.jpg";

function NavBar(props) {
  const { onClick } = props;
  return (
    <React.Fragment>
      <div>
        <img src={image} className="App-logo" alt="logo" />
      </div>
      <div className="App-list">
        <ul>
          {myData.navList.map((nav) => (
            <li onClick={onClick} className="App-nav" key={nav.index}>
              <Link className="App-nav" to={nav}>
                {nav}
              </Link>
            </li>
          ))}
        </ul>
        <hr></hr>
      </div>
    </React.Fragment>
  );
}

export default NavBar;
