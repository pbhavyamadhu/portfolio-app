import React from "react";
import "../styles/organization.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Contact() {
  return (
    <React.Fragment>
      <div className="imgStyle contactApp"></div>
      <div className="contact">
        <div>
          EmailID:{" "}
          <a href="mailto: pbhavyamadhu@gmail.com">pbhavyamadhu@gmail.com</a>
        </div>
        <div>
          Contact Number: <span>813-570-5707</span>
        </div>
        <div>
          LinkedIn:{" "}
          <a href="https://www.linkedin.com/in/bhavya-pittala/">
            linkedin.com/in/bhavya-pittala/
          </a>
        </div>
        <div>
          <FontAwesomeIcon icon="fa-solid fa-phone" />
        </div>
      </div>
    </React.Fragment>
  );
}

export default Contact;
