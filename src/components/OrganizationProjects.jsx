import React, { useState } from "react";
import projectData from "../data/organizationProjectData.json";
import "../styles/organization.css";

function OrganizationProjects() {
  const [data] = useState(projectData);
  return (
    <React.Fragment>
      <div>
        <div className="imgStyle orgaApp"></div>
        <h1>{data.Occupation.title}</h1>
        <table className="table table-bordered">
          <thead className="comp">
            <tr>
              <th>Company Name</th>
              <th>Role</th>
              <th>Description</th>
              <th>Responsibilities</th>
              <th>Environment</th>
            </tr>
          </thead>
          <tbody className="comp">
            {data.Occupation.Company.map((comp) => (
              <tr>
                <td className="date">
                  {comp.name} -<div>{comp.location}</div>
                </td>
                <td className="date">
                  {comp.role}
                  <div>({comp.date})</div>
                </td>
                <td className="desc">{comp.desc}</td>
                <td>
                  <ul className="orgList">
                    {comp.Responsibilities.keyPoints.map((res) => (
                      <li>{res}</li>
                    ))}
                  </ul>
                </td>
                <td>
                  <ul>
                    {comp.Environment.skills.map((env) => (
                      <li>{env}</li>
                    ))}
                  </ul>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
}

export default OrganizationProjects;
