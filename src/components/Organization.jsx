import React from "react";
import projectDetails from "../data/organizationProjectData.json";
import "../styles/organization.css";

function Organiation() {
  return (
    <React.Fragment>
      <div>
        <h1>{projectDetails.title}</h1>
        <ul className="orgList">
          {projectDetails.description.map((desc) => (
            <li>{desc}</li>
          ))}
        </ul>
        <h1>{projectDetails["Educational Qualification"].title}</h1>
        <table className="table">
          {projectDetails["Educational Qualification"].UniversityDetails.map(
            (univ) => (
              <tr>
                <td>{univ.title}</td>
                <td>{univ.Date}</td>
              </tr>
            )
          )}
        </table>
        <h1>{projectDetails.techTitle}</h1>
        <table className="table table-dark">
          <thead>
            <tr>
              <td>Technology</td>
              <td>Tools</td>
            </tr>
          </thead>
          <tbody>
            {projectDetails["Technical Skills"].map((tech) => (
              <tr>
                <td>{tech.technology}</td>
                <td>{tech.areas}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
}

export default Organiation;
