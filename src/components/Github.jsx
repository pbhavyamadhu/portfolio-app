import React from "react";
import "../styles/organization.css";

function Github() {
  return (
    <React.Fragment>
      <div className="imgStyle gitApp"></div>
      <div>
        <h1>Github</h1>
        <div className="contact">
          <a href="https://github.com/Bhavyamadhu2006">
            github.com/Bhavyamadhu2006
          </a>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Github;
