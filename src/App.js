import React from "react";
import { Route, Switch } from "react-router-dom";

import About from "./components/About";
import NavBar from "./components/Navbar";
import OrganizationProjects from "./components/OrganizationProjects";
import Personal from "./components/Personal";
import Github from "./components/Github";
import Contact from "./components/Contact";
import Summary from "./components/Summary";

import "./styles/App.css";

function App() {
  const handleClick = () => {};

  return (
    <div className="build">
      <div className="container">
        <NavBar onClick={handleClick} />
        <div>
          <Switch>
            <Route path="/Summary" component={Summary} />
            <Route
              path="/Organization Projects"
              component={OrganizationProjects}
            />
            <Route path="/Personal Projects" component={Personal} />
            <Route path="/Github" component={Github} />
            <Route path="/Contact" component={Contact} />
            <Route path="/" component={About} />
          </Switch>
        </div>
      </div>
    </div>
  );
}

export default App;
